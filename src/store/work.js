const WORK_LOADING = 'WORK_LOADING';
const WORK_LOADED = 'WORK_LOADED';

export const selectAllWork = state => {
  return state.work.work;
}

export const selectWork = (state, id) => {
  if(state.work && state.work.work && state.work.work[id]){
    return state.work.work[id];
  } else {
    return false;
  }
}

const initState = {
  isLoading: false
}

export default (state = initState, action) => {
  switch (action.type) {
    case WORK_LOADING:
      return {...state, isLoading: true}
    case WORK_LOADED:
      return {...state, isLoading: false, work : action.data}
    default:
      return state
  }
}
