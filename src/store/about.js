const ABOUT_LOADING = 'ABOUT_LOADING';
const ABOUT_LOADED = 'ABOUT_LOADED';

export const selectAbout = state => state.about.about;

const initState = {
  isLoading: false,
  about: null
}

export default (state = initState, action) => {
  switch (action.type) {
    case ABOUT_LOADING:
      return {...state, isLoading: true}
    case ABOUT_LOADED:
      return {...state, isLoading: false, about: action.data}
    default:
      return state
  }
}
