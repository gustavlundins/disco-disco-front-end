const SITE_LOADING = 'SITE_LOADING';
const SITE_LOADED = 'SITE_LOADED';

export const selectConfig = state => state.config;
export const selectMenu = state => state.site.menu;
export const selectTitle = state => state.site.title;

const initState = {
  isLoading: false,
  title: null,
  menu: null
}

export default (state = initState, action) => {
  switch (action.type) {
    case SITE_LOADING:
      return {...state, isLoading: true}
    case SITE_LOADED:
      return {...state, isLoading: false, ...action.data}
    default:
      return state
  }
}
