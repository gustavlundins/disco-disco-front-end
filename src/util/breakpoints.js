import { labels, breakpointMap as breakpoints } from 'lib/theme';

export const findBreakpoint = (label, breakpoints) =>
  breakpoints.find(bp => bp.label === label);

export const getBreakpointValue = label =>
  typeof label === 'object'
    ? label.size
    : findBreakpoint(label, breakpoints).size;

export const getMaxBreakpointValue = label => {
  if (label === null) return;
  /**
   * We do this to prevent collions between breakpoints.
   * https://www.w3.org/TR/mediaqueries-4/#range-context
   */
  const breakpointValue = getBreakpointValue(label).toString();
  const postfix = breakpointValue.match(/[a-zA-Z]+/) || '';
  const value = breakpointValue.match(/[0-9]+/);

  return `${value - 0.01}${postfix}`;
};

export const getNextBreakpoint = breakpoint => {
  let index = breakpoints.indexOf(findBreakpoint(breakpoint, breakpoints));
  return index !== breakpoints.length - 1 ? breakpoints[index + 1] : null;
};

export { labels };
