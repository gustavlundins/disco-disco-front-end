import { getMaxBreakpointValue, getNextBreakpoint } from 'util/breakpoints';

const breakpoints = ['40em', '52em', '75em', '100em', '110em'];
const labels = ['xs', 'sm', 'md', 'lg', 'xl', 'xxl'];

const breakpointMap = [0, ...breakpoints].reduce((arr, size, index) => {
  return [
    ...arr,
    {
      label: labels[index],
      size: size
    }
  ];
}, []);

const space = {
  0: 0,
  1: 6,
  2: 12,
  3: 18,
  4: 24,
  5: 30,
  6: 36,
  7: 42,
  8: 48,
  9: 54,
  10: 60,
  12: 66,
  14: 72
};

const fontSizes = [10, 12, 14, 16, 20, 24, 28, 32, 36, 48, 64, 72];

const fontWeights = [300, 500, 600];

const fontFamilies = {
  serif: 'Optima',
  sans: 'CentraNo2'
};

const lineHeights = [1.5, 1.75];

const colors = {
  darkestGray: '#808080',
  darkGray: '#B2B2B2',
  primaryGray: '#F2F2F2',
  lightGray: '#F7F7F7',
  pink: '#FCE9EB',
  purple: '#AFA2B0',
  blue: '#BBD9FF'
};

const above = breakpointMap.reduce((obj, bp) => {
  return {
    ...obj,
    [bp.label]: `@media (min-width: ${bp.size})`
  };
}, {});

const below = breakpointMap.reduce((obj, bp) => {
  return {
    ...obj,
    [bp.label]: `@media (max-width: ${getMaxBreakpointValue(bp.label)})`
  };
}, {});

const between = breakpointMap.reduce((obj, bp, breakpointMapIndex) => {
  /**
   * Create an array of min - max labels for each breakpoint
   * (xs-md, xs-lg etc)
   */
  const breakpointLabels = labels
    .reduce((arr, label, breakpointLabelIndex) => {
      return [
        ...arr,
        bp.label === label
          ? null
          : breakpointMapIndex < breakpointLabelIndex
            ? { name: `${bp.label}-${label}`, from: bp.label, to: label }
            : null
      ];
    }, [])
    .filter(bp => bp !== null);

  /**
   * Create an array of CSS media queries from the breakpoint labels
   */
  const mediaQueries = breakpointLabels.reduce((obj, bpName) => {
    return {
      ...obj,
      [bpName.name]: `@media (min-width: ${bp.size}) and (max-width: ${
        breakpointMap.find(bp => bp.label === bpName.to).size
      })`
    };
  }, {});

  return {
    ...obj,
    ...mediaQueries
  };
}, {});

const only = breakpointMap.reduce((obj, bp) => {
  let nextBreakpoint = getNextBreakpoint(bp.label);
  let nextSize = nextBreakpoint
    ? getMaxBreakpointValue(nextBreakpoint.label)
    : null;
  return {
    ...obj,
    /**
     * Create min-max queries to target the specific size requested.
     * The last size in the array has no upper limit, so acts the same as
     * `from`
     */
    [bp.label]: nextSize
      ? `@media (min-width: ${bp.size}) and (max-width: ${nextSize})`
      : `@media (min-width: ${bp.size})`
  };
}, {});
export default {
  breakpoints,
  breakpointMap,
  labels,
  space,
  fontSizes,
  colors,
  lineHeights,
  fontWeights,
  fontFamilies,
  above,
  below,
  between,
  only
};

export { breakpointMap, labels };
