import React from 'react';
import { Flex, Text, Box } from 'components/UI/Base'
import { Helmet } from 'react-helmet';
import styled from 'react-emotion';
import { selectAbout } from 'store/about';
import { connect } from 'react-redux';
import { documentToHtmlString } from '@contentful/rich-text-html-renderer';
import { themeGet } from 'styled-system';
import Fade from 'react-reveal/Fade';

const Image = styled('img')`
  width:100%;
  height:auto;
  position:relative;
  z-index:-1;
`
const Title = styled(Box)`
  position:absolute;
  top:20px;
  left:20px;
`
const Wrapper = styled(Flex)`
  ${themeGet('below.md')}{
    .content {
      order:1;
    }
    .image {
      order:0;
    }
  }
`

const About = ({about}) => (
  about ? (
    <Fade>
      <Helmet>
        <title>Disco Disco - About</title>
      </Helmet>
      <Wrapper flexDirection={['column',null,'row']}>
        <Flex className="content" justifyContent="center" css={{flexGrow:0}} flexDirection="column" width={[1,null,1/3]}>
          <Box>
            {about && about.text && about.text.content.map((content, index) => (
              <Box lineHeight="1.5em" mb={3} key={index}>
                <div dangerouslySetInnerHTML={{__html : documentToHtmlString(content)}}></div>
              </Box>
            ))}
          </Box>
        </Flex>
        {about && about.image && (
          <Flex className="image" position="relative" mb={[4,null,0]} css={{flexGrow:0}} pl={[0,null,14]} alignItems="flex-start" width={[1,null,2/3]}>
            <Title>
              <Text color="purple" fontSize="5vw" mb={14} is="h1">{about.title}</Text>
            </Title>
            <Image src={about.image.fields.file.url} />
          </Flex>
        )}
      </Wrapper>
    </Fade>
  ) : null
);

const mapStateToProps = state => ({
  about: selectAbout(state),
})
export default connect(mapStateToProps)(About);
