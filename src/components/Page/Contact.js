import React from 'react';
import { Flex, Text } from 'components/UI/Base'
import { Helmet } from 'react-helmet';
import Fade from 'react-reveal/Fade';

import { themeGet } from 'styled-system';
import styled from 'react-emotion';
import { selectAbout } from 'store/about';
import { connect } from 'react-redux';

const Card = styled(Flex)`
  border: 4px solid ${themeGet('colors.blue')};
  width:400px;
  max-width:100%;
  line-height:1.5em;
  position:relative;
  &:before {
    display:block;
    content:'';
    padding-top:56.25%;
    width:0;
  }
`

const Contact = () => (
  <Fade>
    <Helmet>
      <title>Disco Disco - Contact</title>
    </Helmet>
    <Flex height="100%" py={14} alignItems="center" justifyContent="center">
      <Card>
        <Flex p={2} flexDirection="column">
          <Text>Johan Andersson</Text>
          <Flex mt="auto" flexDirection="column">
            <Text>johan.andersson@gmail.com</Text>
            <Text>+47723988966</Text>
          </Flex>
        </Flex>
      </Card>
    </Flex>
  </Fade>
);

const mapStateToProps = state => ({
  about: selectAbout(state),
})
export default connect(mapStateToProps)(Contact);
