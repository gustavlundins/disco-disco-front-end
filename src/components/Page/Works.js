import React from 'react';
import { Flex, Text, Box } from 'components/UI/Base'
import styled from 'react-emotion';
import { selectAllWork } from 'store/work';
import { connect } from 'react-redux';
import { themeGet } from 'styled-system';
import { Link } from 'react-router-dom';
import Fade from 'react-reveal/Fade';
import ScrollToTop from 'components/UI/ScrollToTop';

const Image = styled('img')`
  width:100%;
  height:auto;
  display:block;
`

const Item = styled(Flex)`
  cursor:pointer;
  span {
    opacity:0;
  }
  &:hover {
    span {
      opacity:1;
    }
  }
  ${themeGet('below.md')}{
    span {
      opacity:1;
    }
  }
`

const ImageWrapper = styled(Box)`
  position:relative;
  &:after {
    content:'';
    display:block;
    top:0;
    left:0;
    right:0;
    bottom:0;
    background:rgba(0,0,0,0);
    position:absolute;
    transition:background .46s cubic-bezier(0.25,0.46,0.45,0.94);
  }
  &:hover {
    &:after {
      background:rgba(0,0,0,0.2);
    }
  }
`

const layout = {
  0: 'wide',
  1: 'wide',
  2: 'third',
  3: 'third',
  4: 'third',
  5: 'third',
  6: 'third',
  7: 'third',
  8: 'wide',
  9: 'wide',
  10: 'wide',
  11: 'wide'
}

const Works = ({work}) => {
  return (
    <ScrollToTop>
      <Flex pb={14} justifyContent="space-between" alignItems="center" width="auto" flexWrap="wrap" mx="-42px">
        {work && work.map((item, index) => (
          <Item className={item.fields.wide ? 'wide' : ''} flexDirection="column" key={index} width={[1,null,layout[index] === 'wide' ? 1/2 : 1/3]} px={7} mb={[7,null,14]}>
            <Fade>
              <Link to={'work/' + index}>
                <ImageWrapper mb={2}>
                  <Image alt={item.fields.image.fields.title} src={item.fields.image.fields.file.url} />
                </ImageWrapper>
              </Link>
              <Text width={1} fontSize={[3,null,null,5]} textAlign="right">
                {item.fields.title}
              </Text>
            </Fade>
          </Item>
        ))}
      </Flex>
    </ScrollToTop>
  )
};

const mapStateToProps = state => ({
  work: selectAllWork(state),
})
export default connect(mapStateToProps)(Works);
