import React from 'react';
import { Flex, Text, Box } from 'components/UI/Base'
import styled from 'react-emotion';
import { selectWork } from 'store/work';
import { connect } from 'react-redux';
import { themeGet } from 'styled-system';
import { Helmet } from 'react-helmet';
import Fade from 'react-reveal/Fade';
import ScrollToTop from 'components/UI/ScrollToTop';

const Image = styled('img')`
  width:100%;
  height:auto;
`

const Title = styled(Box)`
  position:absolute;
  top:20px;
  left:20px;
`

const Wrapper = styled(Flex)`
  ${themeGet('below.md')}{
    .content {
      order:1;
    }
    .image {
      order:0;
      h1 {
        display:none;
      }
    }
  }
`

const Work = ({work}) => {
  if(!work) return null;
  const { description, image, title, year, images } = work.fields;
  return (
    work ? (
      <ScrollToTop>
        <Helmet>
          <title>Disco Disco - {title}</title>
        </Helmet>
        <Fade>
          <Wrapper flexDirection={['column',null,'row']}>
            <Flex className="content" justifyContent="center" css={{flexGrow:0}} flexDirection="column"  width={[1,null,1/3]}>
              {title && (
                <Text mb={3} lineHeight="1.6em" color="black" fontWeight="bold">{title} {year && (`(${year})`)}</Text>
              )}
              {description && (
                <Text lineHeight="1.6em" is="p">{description}</Text>
              )}
            </Flex>
            <Flex className="image" position="relative" pl={[0,null,14]} mb={[2,null,0]} alignItems="flex-start" width={[1,null,2/3]}>
              <Title>
                <Text color="purple"  fontSize="3vw" is="h1">{title}</Text>
              </Title>
              {image && (
                <Image src={image.fields.file.url} />
              )}
            </Flex>
          </Wrapper>
        </Fade>
        {images && (
          <Flex mt={[7,null,14]} flexDirection="column" width={1}>
            <Fade>
              {images.map((image,index) => (
                <Box mb={[7,null,14]}>
                  <Image src={image.fields.file.url} />
                </Box>
              ))}
            </Fade>
          </Flex>
        )}
      </ScrollToTop>
    ) : null
  )
};

const mapStateToProps = (state, props) => {
  let id = null;
  if(props.match && props.match.params){
    id = props.match.params.workId;
  }
  return ({
  work: selectWork(state, id),
})}
export default connect(mapStateToProps)(Work);
