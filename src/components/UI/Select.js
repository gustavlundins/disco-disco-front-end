import React, { Component } from 'react';
import PropTypes from 'prop-types';

const Option = props => <option {...props} />;

class Select extends Component {
  static defaultProps = {
    onChange: null,
    value: undefined,
    className: ''
  };

  static propTypes = {
    onChange: PropTypes.func,
    value: PropTypes.string,
    className: PropTypes.string
  };

  handleChange = e => {
    if (this.props.onChange) {
      this.props.onChange(e);
    }
  };

  render() {
    return (
      <select
        className={this.props.className}
        value={this.props.value}
        onChange={this.handleChange}
      >
        {this.props.children}
      </select>
    );
  }
}

export { Select, Option };
