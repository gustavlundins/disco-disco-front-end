import React from 'react';
import SVGInline from 'react-svg-inline';
import styled from 'react-emotion';

const StyledSvgInline = styled(SVGInline)`
  line-height: ${props => props.height}px;
  display: inline-block;
  svg {
    height:${props => props.height}px;
    width: ${props => props.height}px;
    fill: ${props => props.fill};
  }
  path {
    fill: ${props => props.fill};
    stroke: ${props => props.pathstroke};
  }
  polygon {
    fill: ${props => props.fill};
    stroke: ${props => props.pathstroke};
  }
`;

const Icon = ({ icon, fill, stroke, width = 20, height = 20 }) => {
  const svg = require(`!raw-loader!./${icon}.svg`);
  return (
    <StyledSvgInline
      width={width}
      height={height}
      fill={fill}
      pathstroke={stroke || ''}
      svg={svg}
    />
  );
};
export default Icon;
