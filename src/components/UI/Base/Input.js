import styled from 'react-emotion';
import {
  space,
  fontSize,
  color,
  textAlign,
  lineHeight,
  fontWeight,
  letterSpacing,
  fontFamily,
  width,
  border,
  borderRadius,
  boxShadow
} from 'styled-system';
import tag from 'clean-tag';
import defaultProps from 'recompose/defaultProps';

const Input = styled(tag)`
  ${space};
  ${width};
  ${fontSize};
  ${color};
  ${textAlign};
  ${lineHeight};
  ${fontWeight};
  ${letterSpacing};
  ${fontFamily};
  ${border};
  ${borderRadius};
  ${boxShadow};
  &:-webkit-autofill {
    -webkit-box-shadow: 0 0 0px 1000px #f7f7f7 inset,
      inset 0 1px 3px 0 rgba(0, 0, 0, 0.04);
  }

  &:focus {
    outline: none;
    + label {
      transform: translateY(-145%) scale(0.7);
    }
  }
  &.active {
    + label {
      transform: translateY(-145%) scale(0.7);
    }
  }
`;

Input.propTypes = {
  ...space.propTypes,
  ...width.propTypes,
  ...fontSize.propTypes,
  ...color.propTypes,
  ...textAlign.propTypes,
  ...lineHeight.propTypes,
  ...fontWeight.propTypes,
  ...letterSpacing.propTypes,
  ...fontFamily.propTypes,
  ...border.propTypes,
  ...borderRadius.propTypes,
  ...boxShadow.propTypes
};

export default defaultProps({ is: 'input' })(Input);
