import React from 'react';
import PropTypes from 'prop-types';
import styled from 'react-emotion';
import {
  space,
  fontSize,
  color,
  textAlign,
  lineHeight,
  fontWeight,
  letterSpacing,
  fontFamily,
  width,
  height,
  border,
  borderRadius,
  boxShadow
} from 'styled-system';
import Tag from 'clean-tag';
import defaultProps from 'recompose/defaultProps';

const TextArea = styled(({ resize, ...props }) => <Tag {...props} />)`
  ${space};
  ${width};
  ${height};
  ${fontSize};
  ${color};
  ${textAlign};
  ${lineHeight};
  ${fontWeight};
  ${letterSpacing};
  ${fontFamily};
  ${border};
  ${borderRadius};
  ${boxShadow};
  resize: ${resize => (resize ? 'initial' : 'none')};
`;

TextArea.propTypes = {
  ...space.propTypes,
  ...width.propTypes,
  ...height.propTypes,
  ...fontSize.propTypes,
  ...color.propTypes,
  ...textAlign.propTypes,
  ...lineHeight.propTypes,
  ...fontWeight.propTypes,
  ...letterSpacing.propTypes,
  ...fontFamily.propTypes,
  ...border.propTypes,
  ...borderRadius.propTypes,
  resize: PropTypes.string,
  ...boxShadow.propTypes
};

export default defaultProps({ is: 'textarea', resize: 'none' })(TextArea);
