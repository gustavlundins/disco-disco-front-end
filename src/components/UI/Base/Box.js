import styled from 'react-emotion';
import tag from 'clean-tag';
import {
  space,
  width,
  height,
  fontSize,
  color,
  flex,
  responsiveStyle,
  textAlign,
  alignSelf,
  lineHeight,
  display,
  borders,
  borderColor,
  position,
  borderRadius,
  minWidth,
  maxWidth,
  order
} from 'styled-system';

const Box = styled(tag)`
  ${borders};
  ${borderColor};
  ${space};
  ${width};
  ${minWidth};
  ${maxWidth};
  ${height};
  ${fontSize};
  ${color};
  ${responsiveStyle};
  ${flex};
  ${order};
  ${textAlign};
  ${alignSelf};
  ${lineHeight};
  ${display};
  ${position};
  ${borderRadius};
`;

Box.propTypes = {
  ...borders.propTypes,
  ...borderColor.propTypes,
  ...space.propTypes,
  ...width.propTypes,
  ...minWidth.propTypes,
  ...maxWidth.propTypes,
  ...height.propTypes,
  ...fontSize.propTypes,
  ...color.propTypes,
  ...flex.propTypes,
  ...order.propTypes,
  ...textAlign.propTypes,
  ...alignSelf.propTypes,
  ...lineHeight.propTypes,
  ...display.propTypes,
  ...position.propTypes,
  ...borderRadius.propTypes
};

export default Box;
