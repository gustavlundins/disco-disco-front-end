import React from 'react';

import { Text } from '../Base';

/**
 * Default to an h1, but encourage use of 'is' prop to
 * define heading type.
 *
 * Passes through all styled-system props to Text
 */
export default ({ children, is = 'h1', ...rest }) => (
  <Text fontSize={7} fontWeight={0} is={is} {...rest}>
    {children}
  </Text>
);
