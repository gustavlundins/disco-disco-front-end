import React from 'react';
import { injectGlobal } from 'emotion';
import theme from '../../../lib/theme';

import { Text } from '../Base';

/**
 * We set margins to 0 here for all headings, so that Headings can
 * be responsible for their own margins via props (using styled-system)
 *
 * The underlying DOM element is set to h2 by default, but can be set with
 * the 'is' prop. This allows us to separate style from semantics.
 *
 * Example usage:
 * <Heading mb={2} is="h1">Heading</Heading>
 *
 * Outputs:
 * <h1 style="margin-bottom: 16px; font-weight: 300">Heading</h1>
 *
 * Note that mb is from styled-system. In this case it means margin-bottom
 * is set to 2 on the sizing scale.
 */

injectGlobal`
  h1, h2, h3, h4, h5, h6 {
    margin: ${theme.space[0]};
    font-weight: ${theme.fontWeights[0]};
  }
`;

export default ({ children, is = 'h2', ...rest }) => (
  <Text fontSize={3} fontWeight={0} is={is} {...rest}>
    {children}
  </Text>
);
