const ScrollToTop = props => {
  if (window && document) {
    // Check if browser supports smooth scroll behavior. If true pass in an object with behavior.
    // Otherwise call window.scrollTo as we did before so that we don't break anything.
    if (
      document.documentElement &&
      'scrollBehavior' in document.documentElement.style
    ) {
      window.scrollTo({
        behavior: props.smooth !== false ? 'smooth' : 'auto',
        left: 0,
        top: 0
      });
    } else {
      window.scrollTo(0, 0);
    }
  }
  return (
    props.children
  )
};

export default ScrollToTop;
