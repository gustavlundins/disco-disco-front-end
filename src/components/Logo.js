import React from 'react';
import { Flex } from 'components/UI/Base'
import styled from 'react-emotion';
import logo from 'assets/logo.svg';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { css } from 'react-emotion';

const transitions = css`
  .menu-leave {
    opacity:1
    transform:translateY(0);
  }
  .menu-leave-active {
    opacity:0;
    transition: opacity 0.35s linear;
  }
  width:100%;
`;
const Image = styled('img')`
  max-height:90%;
  width:auto;
`

const Wrapper = styled(Flex)`
  position:fixed;
  top:0;
  left:0;
  right:0;
  bottom:0;
  z-index:10;
`

const Logo = ({ active }) => (
  <div className={transitions}>
    <ReactCSSTransitionGroup
      transitionName="menu"
      transitionEnterTimeout={350}
      transitionLeaveTimeout={560}
    >
      {active && (
        <Wrapper
          width={1}
          height="100vh"
          alignItems="center"
          justifyContent="center"
          bg="pink"
        >
          <Image src={logo} />
        </Wrapper>
      )}
    </ReactCSSTransitionGroup>
  </div>
);

export default Logo;
