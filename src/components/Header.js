import React from 'react';
import { Flex, Box } from 'components/UI/Base'
import { Link } from 'react-router-dom'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import styled from 'react-emotion';
import logo from 'assets/logo.svg';
import { themeGet } from 'styled-system';
import { css } from 'react-emotion';

const transitions = css`
  .menu-enter {
    transform: translateY(-140%);
    .content {
      opacity: 0;
    }
  }
  .menu-enter-active {
    transform: translateY(0);
    transition: transform 0.5s cubic-bezier(0.86, 0, 0.07, 1);

  }
  .menu-leave {
    transform: translateY(0);
  }
  .menu-leave-active {
    transform: translateY(-140%);
    transition: transform 0.3s cubic-bezier(0.86, 0, 0.07, 1);
  }
`;

const Image = styled('img')`
  width:94px;
  height:94px;
  ${themeGet('below.md')}{
    height:62px;
    width:62px;
  }
`

const Item = styled(Box)`
  cursor:pointer;
  &:hover {
    color:${themeGet('colors.darkestGray')};
  }
  a {
    color:black;
    text-decoration:none;
    &:hover {
      text-decoration:underline;
    }
  }
  position:relative;
  top:-5px;

`

const Header = ({active}) => (
  <div className={transitions}>
    <ReactCSSTransitionGroup
      transitionName="menu"
      transitionEnterTimeout={550}
      transitionLeaveTimeout={460}
    >
      {active && (
        <Flex
          py={[3,null,7]}
          width={1}
          alignItems={['center',null,'flex-end']}
          bg="pink"
          flexDirection={['column',null,'row']}
        >
          <Link to="/"><Image src={logo} /></Link>
          <Flex mt={[3,null,0]} ml={[0,null,'auto']}>
            <Item ml={[2,null,14]} fontSize={3}><Link to="/">work</Link></Item>
            <Item ml={[2,null,14]} fontSize={3}><Link to="/about">about</Link></Item>
            <Item ml={[2,null,14]} fontSize={3}><Link to="/contact">contact</Link></Item>
          </Flex>
        </Flex>
      )}
    </ReactCSSTransitionGroup>
  </div>
);

export default Header;
