import React from 'react';
import { Flex, Text } from 'components/UI/Base'
import styled from 'react-emotion';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { css } from 'react-emotion';

const transitions = css`
  .menu-enter {
    transform: translateY(140%);
    .content {
      opacity: 0;
    }
  }
  .menu-enter-active {
    transform: translateY(0);
    transition: transform 0.5s cubic-bezier(0.86, 0, 0.07, 1);

  }
  .menu-leave {
    transform: translateY(0);
  }
  .menu-leave-active {
    transform: translateY(140%);
    transition: transform 0.3s cubic-bezier(0.86, 0, 0.07, 1);
  }
`;
const Wrapper = styled(Flex)`
  position:fixed;
  bottom:0;
  left:0;
  right:0;
`

const Footer = ({footer, active}) => (
  <div className={transitions}>
    <ReactCSSTransitionGroup
      transitionName="menu"
      transitionEnterTimeout={550}
      transitionLeaveTimeout={460}
    >
      {active && (
        <Wrapper
          py={[3,null,4]}
          px={4}
          bg="pink"
          width={1}
          alignItems="center"
          justifyContent="center"
          textAlign="center"
        >
          <Text fontSize={[0,null,3]}>
              {footer}
          </Text>
        </Wrapper>
      )}
    </ReactCSSTransitionGroup>
  </div>
);

export default Footer;
