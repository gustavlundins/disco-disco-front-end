import {createStore, applyMiddleware, combineReducers} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import workReducer from './store/work';
import siteReducer from './store/site';
import aboutReducer from './store/about';
import thunk from 'redux-thunk';

const reducer = combineReducers({
  work: workReducer,
  config: siteReducer,
  about: aboutReducer
})

export default createStore(
  reducer,
  composeWithDevTools(
    applyMiddleware(thunk)
  )
)
