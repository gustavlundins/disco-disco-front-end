import * as contentful from 'contentful';

const WORK_LOADING = 'WORK_LOADING';
const WORK_LOADED = 'WORK_LOADED';
const WORK_LOADED_ERROR = 'WORK_LOADED_ERROR';

const client = contentful.createClient({
  space: 'q9g4gjijqac0',
  accessToken: 'abec4cf64286beaf73395727cd6357840972563e8eebc2537919aad49b29eac7'
})

export const fetchWork = () => async dispatch => {
  dispatch({ type: WORK_LOADING});

  try {
    const entries = await client.getEntries({ content_type: 'work' });
    dispatch({ type: WORK_LOADED, data: entries.items});
  } catch (e) {
    // throw e;
    dispatch({ type: WORK_LOADED_ERROR });
  }
}
