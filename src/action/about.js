import * as contentful from 'contentful';

const ABOUT_LOADING = 'ABOUT_LOADING';
const ABOUT_LOADED = 'ABOUT_LOADED';
const ABOUT_LOADED_ERROR = 'ABOUT_LOADED_ERROR';

const client = contentful.createClient({
  space: 'q9g4gjijqac0',
  accessToken: 'abec4cf64286beaf73395727cd6357840972563e8eebc2537919aad49b29eac7'
})

export const fetchAbout = () => async dispatch => {
  dispatch({ type: ABOUT_LOADING});

  try {
    const about = await client.getEntries({ content_type: 'about' });
    dispatch({ type: ABOUT_LOADED, data: about.items[0].fields});
  } catch (e) {
    // throw e;
    dispatch({ type: ABOUT_LOADED_ERROR });
  }
}
