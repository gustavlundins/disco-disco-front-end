import * as contentful from 'contentful';

const SITE_LOADING = 'SITE_LOADING';
const SITE_LOADED = 'SITE_LOADED';
const SITE_LOADED_ERROR = 'SITE_LOADED_ERROR';

const client = contentful.createClient({
  space: 'q9g4gjijqac0',
  accessToken: 'abec4cf64286beaf73395727cd6357840972563e8eebc2537919aad49b29eac7'
})

export const fetchConfig = () => async dispatch => {
  dispatch({ type: SITE_LOADING});

  try {
    const config = await client.getEntries({ content_type: 'config' });
    dispatch({ type: SITE_LOADED, data: config.items[0].fields});
  } catch (e) {
    // throw e;
    dispatch({ type: SITE_LOADED_ERROR });
  }
}
