import React, { Component } from 'react';
import { Route, Switch, withRouter } from 'react-router';
import { Helmet } from 'react-helmet';

import { Flex } from './components/UI/Base'
import { fetchWork } from 'action/work';
import { fetchConfig } from 'action/site';
import { fetchAbout } from 'action/about';
import { selectConfig } from 'store/site';
import { connect } from 'react-redux';

import Logo from 'components/Logo';
import Footer from 'components/Footer';
import Header from 'components/Header';
import Works from 'components/Page/Works';
import Work from 'components/Page/Work';
import About from 'components/Page/About';
import Contact from 'components/Page/Contact';


class Site extends Component {

  state = {
    isLoaded: false
  }

  componentDidMount = () => {
    this.props.onfetchWork();
    this.props.onfetchConfig();
    this.props.onfetchAbout();
    setTimeout(() => {
      this.setState({
        isLoaded:true
      })
    }, 2000);
  }

  render() {
    const { config } = this.props;
    const { isLoaded } = this.state;

    return (
      <Flex px={[2,'5vw',null,'10vw']} flexDirection="column" width={1}>
        <Helmet>
          <title>Disco Disco</title>
        </Helmet>
        <Header active={isLoaded} />
        <Logo active={!isLoaded} />
        <Switch>
          {/* <Route path="/checkout" component={CheckoutHeader} /> */}
          <Route exact path="/" component={Works} {...this.props} />
          <Route exact path="/about" component={About} {...this.props} />
          <Route exact path="/contact" component={Contact} {...this.props} />
          <Route
            path={`/work/:workId?`}
            component={Work}
          />

        </Switch>
        <Footer active={isLoaded} footer={config.footerText} />
      </Flex>
    );
  }
}

const mapStateToProps = state => ({
  config: selectConfig(state)
})
const mapDispatchToProps = dispatch => ({
  onfetchConfig: () => {
    dispatch(fetchConfig());
  },
  onfetchWork: () => {
    dispatch(fetchWork());
  },
  onfetchAbout: () => {
    dispatch(fetchAbout());
  }
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Site));
