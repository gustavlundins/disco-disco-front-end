import React from 'react';
import { ThemeProvider } from 'emotion-theming';
import theme from 'lib/theme';
import Site from './Site';
import { withRouter } from 'react-router';
import 'assets/reset.css';

const App = () => (
  <ThemeProvider theme={theme}>
    <Site />
  </ThemeProvider>
);

export default withRouter(App);
